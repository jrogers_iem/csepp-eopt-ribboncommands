﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="e0a1e737-2325-43c1-918d-92e39bd63c02" description="Adds the ribbon commands to the Documents and Library tabs for EOPT documents." featureId="e0a1e737-2325-43c1-918d-92e39bd63c02" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="EOPT Ribbon Commands" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="c29ff10b-36cb-4156-8af5-5dba81dd7853" />
  </projectItems>
</feature>